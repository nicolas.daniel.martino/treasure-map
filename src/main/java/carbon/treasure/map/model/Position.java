package carbon.treasure.map.model;

import carbon.treasure.map.Cardinal;

public class Position {

	private int x;
	private int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Position computeNextPosition(Cardinal orientation) {
		return new Position(this.x + orientation.getDirection()[0], this.y + orientation.getDirection()[1]);
	}

	public String getPositionAsKey() {
		return String.valueOf(this.getX()) + "-" + String.valueOf(this.getY());
	}

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
