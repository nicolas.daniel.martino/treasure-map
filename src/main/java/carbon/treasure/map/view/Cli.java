package carbon.treasure.map.view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.Scanner;

import carbon.treasure.map.Game;

public class Cli {

	private File exportFile;
	private File rawMapFile;
	private String exportFilePath;

	public Game createGame(File file) throws IOException {
		this.rawMapFile = this.getFileFromPath(file, "a map file");
		return new Game(this.rawMapFile);
	}

	private File getFileFromPath(File file, String msg) {
		if (file != null) {
			return file;
		}

		final Scanner scanner = new Scanner(new InputStreamReader(System.in));
		System.out.println("Please provide a path to " + msg);
		final String input = scanner.nextLine();

		System.out.println("You have entered:" + input);
		final Path path = Path.of(input);
		System.out.println("Extracting file from path: " + path.getFileName());

		// C:\Users\nicky\Documents\carbon\test1.map.txt
		// C:\Users\nicky\Documents\carbon\testMax.map.txt
		try {
			final File curFile = path.toFile();
			if (curFile.exists()) {
				return curFile;
			}
			System.out.println("Error importing file:\n");
			System.out
					.println("Please provide a path smiliar to: C:\\Users\\youName\\Documents\\carbon\\test1.map.txt");

			return this.getFileFromPath(null, msg);
		} finally {
			scanner.close();
		}

	}

	public void exportReport(String report) throws IOException {
		System.out.println("Generating game report...");
		final String fileSeparator = System.getProperty("file.separator");
		final String newFileName = removeFileExtension(this.rawMapFile.getName(), true) + ".report";
		this.exportFilePath = this.rawMapFile.getParentFile().getAbsolutePath() + fileSeparator + newFileName;
		// creates a report directory
		final File file = new File(this.exportFilePath);

		if (file.createNewFile()) {
			System.out.println("File created: " + file.getName());
		} else {
			System.out.println("File already exist: " + file.getName());
		}
		System.out.println("File path: " + file.getAbsolutePath());

		final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(report);

		writer.close();
	}

	public File getExportFile() {
		return this.exportFile;
	}

	public void setExportFile(File exportPath) {
		this.exportFile = exportPath;
	}

	/**
	 * Taken from https://www.baeldung.com/java-filename-without-extension
	 *
	 * @param filename
	 * @param removeAllExtensions
	 * @return
	 */
	private static String removeFileExtension(String filename, boolean removeAllExtensions) {
		if (filename == null || filename.isEmpty()) {
			return filename;
		}

		final String extPattern = "(?<!^)[.]" + (removeAllExtensions ? ".*" : "[^.]*$");
		return filename.replaceAll(extPattern, "");
	}

	public String getExportFilePath() {
		return this.exportFilePath;
	}
}
