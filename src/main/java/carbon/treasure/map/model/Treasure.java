package carbon.treasure.map.model;

import java.util.Optional;

/**
 * Inmovable gameObject that contains the parsing function to create the
 * mountain
 *
 * @author nicky
 *
 */
public class Treasure extends GameObject {

	private int count = 1;

	public Treasure() {
		super("Treasure", "A quantity of precious metals, gems, or other valuable objects.", false, true, "T", 2);
		// TODO Auto-generated constructor stub
	}

	public int getCount() {
		return this.count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void incrementCount() {
		this.count++;
	}

	public void decrementCount() {
		this.count--;
	}

	@Override
	public boolean addToTile(Tile tile) {
		final Optional<GameObject> t = tile.getContent().stream().filter(i -> i instanceof Treasure).findAny();
		if (t.isPresent()) {
			((Treasure) t.get()).incrementCount();
			return true;
		}
		return super.addToTile(tile);
	}

	@Override
	public boolean deleteFromTile(Tile tile) {
		/// Should destroy tile if empty?
		final Optional<GameObject> tOpt = tile.getContent().stream().filter(i -> i instanceof Treasure).findAny();
		if (!tOpt.isPresent()) {
			return super.deleteFromTile(tile);
		}
		final Treasure t = (Treasure) tOpt.get();
		t.decrementCount();
		if (t.getCount() == 0) {
			tile.getContent().remove(this);
		}
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " - " + this.count;
	}

}
