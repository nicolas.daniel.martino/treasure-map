package carbon.treasure.map.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import carbon.treasure.map.CanGetReward;

public class Map {

	private String[] rawMap;
	private java.util.Map<String, Tile> tiles = new HashMap<>();
	private int width;
	private int height;

	public Map(String[] splittedLine) {
		this.createMap(splittedLine);
	}

	public void printCurrentMap() {
		System.out.println(this.getMapAsString());
	}

	public String getMapAsString() {
		String mapAsString = "";
		final int maxLineSize = this.maxTresaureOnOneTile() + 3; // 3 for the line character and the parenthesis
		final int paddedLineSize = maxLineSize + 2;

		for (int i = 0; i < this.height; i++) {
			String line = "";
			for (int j = 0; j < this.width; j++) {
				final Position position = new Position(j, i);
				final Tile tile = this.tiles.get(position.getPositionAsKey());
				if (tile == null || tile.getContent().size() == 0) {
					line += this.getPaddedAndCenteredString(".", paddedLineSize, ' ');
				} else {
					for (final GameObject go : tile.getContent()) {
						String s = "";
						if (go instanceof Treasure) {
							s += " " + go.getLineCharacter() + "(" + ((Treasure) go).getCount() + ")";
						} else {
							s += " " + go.getLineCharacter() + " ";
						}
						line += this.getPaddedAndCenteredString(s, paddedLineSize, ' ');
					}
				}
			}
			mapAsString += line + "\n";
		}
		return mapAsString;
	}

	private String getPaddedAndCenteredString(String s, int maxLineSize, Character pad) {

		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < (maxLineSize - s.length()) / 2; i++) {
			sb.append(pad);
		}
		sb.append(s);
		while (sb.length() < maxLineSize) {
			sb.append(pad);
		}

		return sb.toString();
	}

	public int maxTresaureOnOneTile() {
		int maxTreasure = 0;
		for (final GameObject go : this.getRemainingGameObject()) {
			if (go instanceof Treasure) {
				final int treasureCount = ((Treasure) go).getCount();
				if (treasureCount > maxTreasure) {
					maxTreasure = treasureCount;
				}
			}
		}
		return maxTreasure;
	}

	public void setRawMap(String[] splittedLine) {
		this.rawMap = splittedLine;
	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean addGameObjectOnTile() {
		return false;

	}

	public void createMap(String[] splittedLine) {
		this.setRawMap(splittedLine);
		this.setWidth(Integer.parseInt(splittedLine[1]));
		this.setHeight(Integer.parseInt(splittedLine[2]));
	}

	/**
	 * Tries to find a tile and if the tile does not exist but could creates it
	 *
	 * @param position
	 * @return
	 */
	public Tile findOneTileOrCreateIt(Position position) {

		// If the tile can exist
		if (this.tileCanExist(position)) {
			final String tileKey = position.getPositionAsKey();
			if (this.tiles.containsKey(tileKey)) {
				return this.tiles.get(tileKey);
			}
			// Otherwise creates the tile
			this.tiles.put(tileKey, new Tile());
			return this.tiles.get(tileKey);
		}
		return null;
	}

	public boolean tileCanExist(Position position) {
		return position.getX() < this.width && position.getX() >= 0 && position.getY() < this.height
				&& position.getY() >= 0;
	}

	public void addGameObject(GameObject go) throws Exception {
		if (!this.canMoveTo(go, go.getPosition())) {
			throw new Exception("An obstacle prevented to place " + go.getName() + " on the tile "
					+ this.findOneTileOrCreateIt(go.getPosition()));
		}
		go.addToTile(this.findOneTileOrCreateIt(go.getPosition()));
	}

	public boolean canMoveTo(GameObject go, Position position) {
		if (!this.tileCanExist(position)) {
			return false;
		}
		if (this.tiles.containsKey(position.getPositionAsKey())) {
			final List<GameObject> goListOnTile = this.findOneTileOrCreateIt(position).getContent();
			final boolean hasNoObstacle = !goListOnTile.stream().anyMatch(GameObject::isObstacle);
			final boolean hasNoAdventurer = !goListOnTile.stream().anyMatch(goOnTile -> goOnTile instanceof Adventurer);
			return hasNoAdventurer && hasNoObstacle;
		}
		return true;

	}

	public boolean moveGameObject(GameObject go, Position newPosition) throws Exception {
		// canMove to new Location
		final boolean canMove = this.canMoveTo(go, newPosition);
		if (!canMove) {
			return false;
		}
		// erase piece from set at location
		final Tile tileFrom = this.findOneTileOrCreateIt(go.getPosition());
		final boolean wasRemoved = go.deleteFromTile(tileFrom);

		if (!wasRemoved) {
			// TODO refactor exceptions in custom exception mngmt
			throw new Exception("Could not move piece " + go.getName() + " from the tile "
					+ this.findOneTileOrCreateIt(go.getPosition()));
		}
		// add piece to new location
		final Tile tileTo = this.findOneTileOrCreateIt(newPosition);
		go.addToTile(tileTo);

		final Optional<Treasure> tileHasReward = tileTo.getContent().stream().filter(i -> i instanceof Treasure)
				.findFirst().map(i -> (Treasure) i);

		// reward mngmt
		if (tileHasReward.isPresent() && go instanceof CanGetReward) {
			final CanGetReward rewardGetter = (CanGetReward) go;
			final Treasure reward = tileHasReward.get();
			rewardGetter.addOneReward();
			// erase reward for future passes
			final boolean hasRemovedReward = reward.deleteFromTile(tileTo);
			if (!hasRemovedReward) {
				throw new Exception("Could not remove reward " + reward.getName() + " from the tile "
						+ this.findOneTileOrCreateIt(go.getPosition()));
			}
		}

		return true;

	}

	@Override
	public String toString() {
		return "C" + " - " + this.width + " - " + this.height;
	}

	/**
	 * Returns a list containing all the gameObjects remaining on the map
	 *
	 * @return
	 */
	public List<GameObject> getRemainingGameObject() {
		final List<GameObject> goRemaining = new ArrayList<GameObject>();
		for (final Tile tile : this.getTiles().values()) {
			if (tile.getContent().size() != 0) {
				goRemaining.addAll(tile.getContent());
			}
		}
		return goRemaining;
	}

	public String[] getRawMap() {
		return this.rawMap;
	}

	public java.util.Map<String, Tile> getTiles() {
		return this.tiles;
	}

	public void setTiles(java.util.Map<String, Tile> tiles) {
		this.tiles = tiles;
	}

}
