package carbon.treasure.map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import carbon.treasure.map.model.Adventurer;
import carbon.treasure.map.model.GameObject;
import carbon.treasure.map.model.Map;
import carbon.treasure.map.model.Mountain;
import carbon.treasure.map.model.Move;
import carbon.treasure.map.model.Position;
import carbon.treasure.map.model.Tile;
import carbon.treasure.map.model.Treasure;
import carbon.treasure.map.view.Cli;

/**
 * Unit test for simple App.
 */
public class AppTest {
	// Helper functions
	/**
	 * Create a simple test environement with a map from resource file
	 *
	 * @return
	 * @throws IOException
	 */
	private Game createSimpleTestEnv() throws IOException {
		final File file = new File("src/test/resources/test1.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);
		return game;
	}

	/**
	 * Create a test env with a map and some game object from resource file
	 *
	 * @return
	 * @throws IOException
	 */
	private Game createTestEnvWithGameObjects() throws IOException {
		final File file = new File("src/test/resources/test2.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);
		return game;
	}

	/**
	 * Asserts if the gameObject is placed correctly on the map
	 *
	 * @param go
	 * @param map
	 */
	private void wellPositionnedOnMap(GameObject go, Map map) {
		final List<GameObject> goObjectInMap = map.findOneTileOrCreateIt(go.getPosition()).getContent();
		assertTrue(goObjectInMap.contains(go));
	}

	/**
	 * Counts the number of rewards in a given map
	 *
	 * @param map
	 * @return
	 */
	private int numberOfRewardsOnMap(Map map) {
		int rewards = 0;
		for (final Tile tile : map.getTiles().values()) {
			for (final GameObject go : tile.getContent()) {
				if (go instanceof Treasure) {
					rewards += ((Treasure) go).getCount();
				}
			}
		}
		return rewards;
	}

	// Tests

	@Test
	public void testImportGameFiles() throws IOException {
		final Game game = this.createSimpleTestEnv();
		assertEquals(82, game.getRawGameFile().length());
	}

	@Test
	public void testRemoveCmt() throws IOException {
		final Game game = this.createSimpleTestEnv();
		final List<String> res = game.removeComments(game.getRawGameFile());
		assertTrue(res.get(0).equals("C - 3 - 4"));
	}

	@Test
	public void testGetMapSize() throws IOException {
		final Game game = this.createSimpleTestEnv();
		try {
			game.createFromFile();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(3, game.getMap().getWidth());
		assertEquals(4, game.getMap().getHeight());
	}

	@Test
	public void printMap() throws Exception {
		Game game = this.createTestEnvWithGameObjects();
		game.createFromFile();

		Map map = game.getMap();
		assertEquals(
				"   .       M       .    \n   .       A       M    \n   .       .       .    \n  T(2)    T(3)     .    \n",
				map.getMapAsString());

		final File file = new File("src/test/resources/simpleMapWithObstacleAndReward.map");
		final Cli gameIO = new Cli();
		game = gameIO.createGame(file);

		game.createFromFile();

		map = game.getMap();
		assertEquals("  .    T(1)  T(1) \n  .     A    T(1) \n  .     M     .   \n  .     .     .   \n",
				map.getMapAsString());
		map.printCurrentMap();

	}

	@Test
	public void testGetTileFromPosition() throws IOException {
		final Game game = this.createSimpleTestEnv();
		try {
			game.createFromFile();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(4, game.getMap().getHeight());
		assertEquals(3, game.getMap().getWidth());
		final Mountain mountain = new Mountain();
		final Tile mountainTile = new Tile();
		mountainTile.addContent(mountain);
		final Position mountainPosition = new Position(2, 3);
		game.getMap().getTiles().put(mountainPosition.getPositionAsKey(), mountainTile);

		final Tile updatedTile = game.getMap().findOneTileOrCreateIt(mountainPosition);
		assertEquals(mountain.getName(), updatedTile.getContent().get(0).getName());
	}

	@Test
	public void testCreateGameObjects() throws IOException {
		final Game game = this.createTestEnvWithGameObjects();
		try {
			game.createFromFile();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(3, Game.getGameObjectsList().size());

		// creates two mountains from file
		final GameObject mountain = game.getGameObjectsInstancesList().stream().filter(i -> i instanceof Mountain)
				.findFirst().orElseThrow();
		assertEquals(1, mountain.getPosition().getX());
		assertEquals(0, mountain.getPosition().getY());
		this.wellPositionnedOnMap(mountain, game.getMap());

		// create 5 treasures from file
		final GameObject t = game.getGameObjectsInstancesList().stream().filter(i -> i instanceof Treasure).findFirst()
				.orElseThrow();
		assertEquals(0, t.getPosition().getX());
		assertEquals(3, t.getPosition().getY());
		this.wellPositionnedOnMap(t, game.getMap());

		// create adventurers
		final Adventurer a = (Adventurer) game.getGameObjectsInstancesList().stream()
				.filter(i -> i instanceof Adventurer).findFirst().orElseThrow();

		assertEquals(1, a.getPosition().getX());
		assertEquals(1, a.getPosition().getY());
		assertEquals(Cardinal.S, a.getOrientation());
		assertEquals(9, a.getMovesList().size());

		// check number of turns from map
		assertEquals(a.getTotalTurns(), game.getTotalTurns());
		this.wellPositionnedOnMap(a, game.getMap());

		assertEquals(1, game.getMoveableGameObjectsList().size());

	}

	@Test
	public void getRessourceFile() {
		final Path path = Path.of("src", "test", "resources");
		assertTrue(Files.exists(path));
	}

	@Test
	public void computeNextOrientation() {
		Cardinal orientation = Cardinal.N;
		Move move = Move.D;
		assertEquals(Cardinal.E, Cardinal.computeNexOrientation(orientation, move));

		orientation = Cardinal.S;
		move = Move.D;
		assertEquals(Cardinal.O, Cardinal.computeNexOrientation(orientation, move));

		orientation = Cardinal.E;
		move = Move.G;
		assertEquals(Cardinal.N, Cardinal.computeNexOrientation(orientation, move));

		orientation = Cardinal.O;
		move = Move.G;
		assertEquals(Cardinal.S, Cardinal.computeNexOrientation(orientation, move));
	}

	@Test
	public void computeNextPosition() {
		Cardinal orientation = Cardinal.N;
		Position p = new Position(0, 0);
		Position expectedPosition = new Position(0, -1);
		assertEquals(expectedPosition.getX(), p.computeNextPosition(orientation).getX());
		assertEquals(expectedPosition.getY(), p.computeNextPosition(orientation).getY());

		orientation = Cardinal.S;
		p = new Position(0, 0);
		expectedPosition = new Position(0, 1);
		assertEquals(expectedPosition.getX(), p.computeNextPosition(orientation).getX());
		assertEquals(expectedPosition.getY(), p.computeNextPosition(orientation).getY());

		orientation = Cardinal.E;
		p = new Position(0, 0);
		expectedPosition = new Position(1, 0);
		assertEquals(expectedPosition.getX(), p.computeNextPosition(orientation).getX());
		assertEquals(expectedPosition.getY(), p.computeNextPosition(orientation).getY());

		orientation = Cardinal.O;
		p = new Position(0, 0);
		expectedPosition = new Position(-1, 0);
		assertEquals(expectedPosition.getX(), p.computeNextPosition(orientation).getX());
		assertEquals(expectedPosition.getY(), p.computeNextPosition(orientation).getY());

		orientation = Cardinal.E;
		p = new Position(32, -5);
		expectedPosition = new Position(33, -5);
		assertEquals(expectedPosition.getX(), p.computeNextPosition(orientation).getX());
		assertEquals(expectedPosition.getY(), p.computeNextPosition(orientation).getY());

	}

	@Test
	public void testBuggyMap() throws IOException {
		final File file = new File("src/test/resources/test_buggy.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);
		assertThrows(Exception.class, () -> game.createFromFile());
	}

	@Test
	public void testGetOffMap() throws Exception {
		// l'aventurier tourne en rond sur la carte
		final File file = new File("src/test/resources/getOffMap.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);

		game.createFromFile();

		final Adventurer lara = (Adventurer) game.getMoveableGameObjectsList().get(0);
		// compute all next turns
		for (int i = 1; i < game.getTotalTurns(); i++) {
			game.computeNextTurn(i);
		}

		assertEquals(1, lara.getPosition().getX());
		assertEquals(3, lara.getPosition().getY());

	}

	@Test
	public void testGameSimpleMapNoObstacles() throws Exception {
		// l'aventurier tourne en rond sur la carte
		final File file = new File("src/test/resources/simpleMapNoObstacles.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);

		game.createFromFile();

		assertEquals(3, game.getMap().getWidth());
		assertEquals(4, game.getMap().getHeight());
		assertEquals(1, game.getMap().getTiles().size());
		assertEquals(1, game.getGameObjectsInstancesList().size());
		assertEquals(1, game.getMoveableGameObjectsList().size());

		final Adventurer lara = (Adventurer) game.getMoveableGameObjectsList().get(0);
		final Position startingPosition = new Position(lara.getPosition().getX(), lara.getPosition().getY());

		final Map map = game.getMap();
		// let's compute the first turn
		game.computeNextTurn(0);

		// first move from (1,1) S to (1,2) S
		assertEquals(Cardinal.S, lara.getOrientation());
		assertEquals(1, lara.getPosition().getX());
		assertEquals(2, lara.getPosition().getY());
		Tile tile = map.findOneTileOrCreateIt(lara.getPosition());
		// map was correctly updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));

		// compute all next turns
		for (int i = 1; i < game.getTotalTurns(); i++) {
			game.computeNextTurn(i);
		}

		assertEquals(Cardinal.O, lara.getOrientation()); // finish by step not turn so facing "ouest"
		assertEquals(startingPosition.getX(), lara.getPosition().getX());
		assertEquals(startingPosition.getY(), lara.getPosition().getY());
		tile = map.findOneTileOrCreateIt(lara.getPosition());
		// map was correctly updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));
	}

	@Test
	public void testGameSimpleMapWithObstacles() throws Exception {
		// l'aventurier tourne en rond sur la carte
		final File file = new File("src/test/resources/simpleMapWithObstacle.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);

		game.createFromFile();

		assertEquals(3, game.getMap().getWidth());
		assertEquals(4, game.getMap().getHeight());
		assertEquals(2, game.getMap().getTiles().size());
		assertEquals(2, game.getGameObjectsInstancesList().size());
		assertEquals(1, game.getMoveableGameObjectsList().size());

		final Adventurer lara = (Adventurer) game.getMoveableGameObjectsList().get(0);
		final Position startingPosition = new Position(lara.getPosition().getX(), lara.getPosition().getY());

		final Map map = game.getMap();
		// let's compute the first turn
		game.computeNextTurn(0);

		// first move from (1,1) S to (1,2) S where obstacle so lara should not move
		assertEquals(Cardinal.S, lara.getOrientation());
		assertEquals(1, lara.getPosition().getX());
		assertEquals(1, lara.getPosition().getY());
		Tile tile = map.findOneTileOrCreateIt(lara.getPosition());
		// map was correctly updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));

		// compute all next turns
		for (int i = 1; i < game.getTotalTurns(); i++) {
			game.computeNextTurn(i);
		}

		assertEquals(Cardinal.S, lara.getOrientation()); // finish by step not turn so facing "ouest"
		assertEquals(startingPosition.getX(), lara.getPosition().getX());
		assertEquals(startingPosition.getY(), lara.getPosition().getY());
		tile = map.findOneTileOrCreateIt(lara.getPosition());
		// map was correctly updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));
	}

	@Test
	public void testGameSimpleMapNoObstaclesWithRewards() throws Exception {
		// l'aventurier tourne en rond sur la carte
		final File file = new File("src/test/resources/simpleMapNoObstaclesWithRewards.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);

		game.createFromFile();

		assertEquals(3, game.getMap().getWidth());
		assertEquals(4, game.getMap().getHeight());
		assertEquals(2, game.getMap().getTiles().size());
		assertEquals(4, game.getGameObjectsInstancesList().size());
		assertEquals(1, game.getMoveableGameObjectsList().size());

		final Map map = game.getMap();
		final Adventurer lara = (Adventurer) game.getMoveableGameObjectsList().get(0);
		final Position startingPosition = new Position(lara.getPosition().getX(), lara.getPosition().getY());
		final Tile rewardTile = map.findOneTileOrCreateIt(new Position(1, 2));

		assertEquals(1, rewardTile.getContent().stream().filter(i -> i instanceof Treasure).collect(Collectors.toList())
				.size());

		// number of treasures on tile
		assertEquals(3, this.numberOfRewardsOnMap(map));

		// let's compute the first turn
		game.computeNextTurn(0);

		// first move from (1,1) S to (1,2) S
		assertEquals(Cardinal.S, lara.getOrientation());
		assertEquals(1, lara.getPosition().getX());
		assertEquals(2, lara.getPosition().getY());
		Tile tile = map.findOneTileOrCreateIt(lara.getPosition());
		// map was correctly updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));

		// compute all next turns
		for (int i = 1; i < game.getTotalTurns(); i++) {
			game.computeNextTurn(i);
		}

		assertEquals(Cardinal.O, lara.getOrientation()); // finish by step not turn so facing "ouest"
		assertEquals(startingPosition.getX(), lara.getPosition().getX());
		assertEquals(startingPosition.getY(), lara.getPosition().getY());
		tile = map.findOneTileOrCreateIt(lara.getPosition());
		// map was correctly updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));

		// at the end of the game the adventurer should have collected one reward
		assertEquals(1, lara.getCurrentRewards());
		// The rward tile should have one reward less
		assertEquals(2, this.numberOfRewardsOnMap(map));

		final String report = game.generateGameReport();
		assertEquals("C - 3 - 4\nT - 1 - 2 - 2\nA - 1 - 1 - O - 1\n", report);
	}

	@Test
	public void testGameOneStepOneReward() throws Exception {
		final File file = new File("src/test/resources/simpleMapWithObstacleAndReward.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);

		game.createFromFile();

		assertEquals(3, game.getMap().getWidth());
		assertEquals(4, game.getMap().getHeight());
		assertEquals(5, game.getMap().getTiles().size());
		assertEquals(5, game.getGameObjectsInstancesList().size());
		assertEquals(1, game.getMoveableGameObjectsList().size());

		final Map map = game.getMap();
		final Adventurer lara = (Adventurer) game.getMoveableGameObjectsList().get(0);
		final Position startingPosition = new Position(lara.getPosition().getX(), lara.getPosition().getY());

		// total number of rewards
		assertEquals(3, this.numberOfRewardsOnMap(map));

		// let's compute the first turn
		game.computeNextTurn(0);

		// first move from (1,1) S to (1,0) S
		assertEquals(Cardinal.S, lara.getOrientation());
		assertEquals(1, lara.getPosition().getX());
		assertEquals(1, lara.getPosition().getY());
		Tile tile = map.findOneTileOrCreateIt(lara.getPosition());
		// map was correctly updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));

		// compute all next turns
		for (int i = 1; i < game.getTotalTurns(); i++) {
			game.computeNextTurn(i);
		}

		assertEquals(Cardinal.S, lara.getOrientation()); // finish by step not turn so facing "ouest"
		assertEquals(startingPosition.getX(), lara.getPosition().getX());
		assertEquals(startingPosition.getY(), lara.getPosition().getY());
		tile = map.findOneTileOrCreateIt(lara.getPosition());
		// map was correctly updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));

		// at the end of the game the adventurer should have collected all rewards
		assertEquals(3, lara.getCurrentRewards());
		// At the end the map has not rewards left to give
		assertEquals(0, this.numberOfRewardsOnMap(map));

		final String report = game.generateGameReport();
		assertEquals("C - 3 - 4\nM - 1 - 2\nA - 1 - 1 - S - 3\n", report);

	}

	@Test
	public void testMultiplayer() throws Exception {
		final File file = new File("src/test/resources/test4.map");
		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);

		game.createFromFile();

		assertEquals(3, game.getMap().getWidth());
		assertEquals(4, game.getMap().getHeight());
		assertEquals(6, game.getMap().getTiles().size());
		assertEquals(2, game.getMoveableGameObjectsList().size());

		final Map map = game.getMap();
		final Adventurer lara = game.getMoveableGameObjectsList().stream().map(i -> (Adventurer) i)
				.filter(i -> i.getFirstName().equals("Lara")).findAny().orElseThrow();
		final Position startingPositionLara = new Position(lara.getPosition().getX(), lara.getPosition().getY());

		final Adventurer jack = game.getMoveableGameObjectsList().stream().map(i -> (Adventurer) i)
				.filter(i -> i.getFirstName().equals("Jack")).findAny().orElseThrow();
		final Position startingPositionJack = new Position(jack.getPosition().getX(), jack.getPosition().getY());

		// total number of rewards
		assertEquals(5, this.numberOfRewardsOnMap(map));

		// let's compute the first turn
		game.computeNextTurn(0);

		// first move from (1,1) S to (1,0) S impeded by Jack
		assertEquals(Cardinal.S, lara.getOrientation());
		assertEquals(1, lara.getPosition().getX());
		assertEquals(1, lara.getPosition().getY());

		assertEquals(Cardinal.N, jack.getOrientation());
		assertEquals(1, jack.getPosition().getX());
		assertEquals(2, jack.getPosition().getY());

		final Tile tile = map.findOneTileOrCreateIt(startingPositionLara);
		// map was not updated
		assertTrue(tile.getContent().stream().anyMatch(i -> i.equals(lara)));

		final Tile tile2 = map.findOneTileOrCreateIt(startingPositionJack);
		// map was not updated
		assertTrue(tile2.getContent().stream().anyMatch(i -> i.equals(jack)));
	}

	@Test
	public void testGameReport() throws Exception {
		final File file = new File("src/test/resources/finalMap.map");

		final Cli gameIO = new Cli();
		final Game game = gameIO.createGame(file);

		final File exportPath = Paths.get("").toFile();
		gameIO.setExportFile(exportPath);
		System.out.println("Report generated in " + exportPath.getAbsolutePath());

		game.createFromFile();
		assertEquals(8, game.getGameObjectsInstancesList().size());
		assertEquals(1, game.getMoveableGameObjectsList().size());

		game.computeAllTurns();
		final String report = game.generateGameReport();
		assertEquals("C - 3 - 4\nM - 1 - 0\nM - 2 - 1\nT - 1 - 3 - 2\nA - 0 - 3 - S - 3\n", report);

		gameIO.exportReport(report);

		String data = "";
		try (BufferedReader br = new BufferedReader(new FileReader(gameIO.getExportFilePath()))) {
			String line;
			while ((line = br.readLine()) != null) {
				data += line + "\n";
			}
		}

		assertEquals("C - 3 - 4\nM - 1 - 0\nM - 2 - 1\nT - 1 - 3 - 2\nA - 0 - 3 - S - 3\n", data);
	}

}
