package carbon.treasure.map;

public interface CanGetReward {

	public int getCurrentRewards();

	public void addOneReward();
}
