# This is an exercise project: Treasure map game.

## Run prj
compile and run target files using java 11 jdk

## Input file type:
C - 3 - 4<br/>
M - 1 - 0<br/>
M - 2 - 1<br/>
T - 0 - 3 - 2<br/>
T - 1 - 3 - 3<br/>
A - Lara - 1 - 1 - S - AADADAGGA

## Explanation
C - 3 - 4 => map of width 3 tiles and height 4 tiles<br/>
M - 1 - 0 => mountain at location (1,0)<br/>
T - 1 - 3 - 3 => treasure at location (1,3) with a count of 3<br/>
A - Lara - 1 - 1 - S - AADADAGGA => Adventurer Lara starting at position (1,1) and orientation S taking steps AADADAGGA<br/>

Steps: AADADAGGA => taken by turns A for forward one tile, G for chage orientation to left; D change orientation to right.

Report files are saved in the same directory with report extension