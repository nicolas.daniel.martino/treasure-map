package carbon.treasure.map;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import carbon.treasure.map.model.Adventurer;
import carbon.treasure.map.model.GameObject;
import carbon.treasure.map.model.GameObjectFactory;
import carbon.treasure.map.model.Map;
import carbon.treasure.map.model.Mountain;
import carbon.treasure.map.model.Position;
import carbon.treasure.map.model.Treasure;

public class Game {

	private String rawGameFile;
	private static List<GameObject> gameObjectsBlueprintList = new ArrayList<>();
	private final List<GameObject> gameObjectsInstancesList = new ArrayList<>();
	private final List<CanMove> moveableGameObjectsList = new ArrayList<>();
	private int totalTurns = 0;

	private Map map;

	public Game(File rawMap) throws IOException {
		this.rawGameFile = Files.readString(rawMap.toPath());
	}

	static {
		gameObjectsBlueprintList.add(new Mountain());
		gameObjectsBlueprintList.add(new Treasure());
		gameObjectsBlueprintList.add(new Adventurer());
	}

	/**
	 * Parses a game file to create a map, creates game objects and adds them to the
	 * map
	 *
	 * @throws Exception
	 */
	public void createFromFile() throws Exception {
		final GameObjectFactory factory = new GameObjectFactory();

		if ((this.rawGameFile == null) || this.rawGameFile.isEmpty()) {
			throw new Exception("Raw map not found");
		}
		// get raw lines without comments
		final List<String> gameFileAsString = this.removeComments(this.rawGameFile);

		gameFileAsString.forEach(item -> {
			System.out.println(item);
		});

		gameFileAsString.forEach(line -> {
			final String[] splittedLine = line.split("-");
			final String[] cleanedSplittedLine = new String[splittedLine.length];
			// clean splittedLine
			for (var i = 0; i < splittedLine.length; i++) {
				cleanedSplittedLine[i] = splittedLine[i].trim();
			}

			if (cleanedSplittedLine[0].toUpperCase().equals("C")) {
				if (this.map == null) {
					this.map = new Map(cleanedSplittedLine);
				}
			}

			// parseGameObjects
			for (final GameObject blueprint : Game.gameObjectsBlueprintList) {
				if (cleanedSplittedLine[0].toUpperCase().equals(blueprint.getLineCharacter())) {
					final List<GameObject> goInstanceList = factory.createFromRawLine(blueprint, cleanedSplittedLine);
					this.gameObjectsInstancesList.addAll(goInstanceList);
				}
			}

		});

		// Placer game objects & add to motile game objects to list & refresh total of
		// turns
		for (final GameObject go : this.gameObjectsInstancesList) {
			this.map.addGameObject(go);
			if (go instanceof CanMove) {
				final CanMove motileObject = (CanMove) go;
				this.moveableGameObjectsList.add(motileObject);
				this.refreshTotalTurns(motileObject.getTotalTurns());
			}
		}
	}

	public void computeAllTurns() throws Exception {
		for (int i = 0; i < this.totalTurns; i++) {
			this.computeNextTurn(i);
		}
	}

	public boolean computeNextTurn(int turn) throws Exception {
		// pour chaque canMove on calcule sa position future
		for (final CanMove moveablePiece : this.moveableGameObjectsList) {
			final Position newPosition = moveablePiece.computeNextMove(turn);
			if (newPosition != null && moveablePiece instanceof GameObject) {
				final GameObject go = (GameObject) moveablePiece;
				final boolean hasMoved = this.map.moveGameObject(go, newPosition);
				if (hasMoved) {
					go.setPosition(newPosition);
				}
			}
		}
		return false;
	}

	public String generateGameReport() {
		// get objects remaining on map
		final List<GameObject> goRemaining = this.map.getRemainingGameObject();
		String report = "";
//		// add map
		report += this.map.toString() + "\n";
		Collections.sort(goRemaining, GameObject.orderForReport());
		for (final GameObject gameObject : goRemaining) {
			report += gameObject.toString() + "\n";
		}
		System.out.println("\nReport :\n" + report);
		return report;

	}

	private void refreshTotalTurns(int nextTurns) {
		this.totalTurns = nextTurns > this.totalTurns ? nextTurns : this.totalTurns;
	}

	public List<String> removeComments(String rawMap) {
		return rawMap.lines().filter(s -> !s.matches("#.*")).collect(Collectors.toList());
	}

	public String getRawGameFile() {
		return this.rawGameFile;
	}

	public void setRawGameFile(String rawGameFile) {
		this.rawGameFile = rawGameFile;
	}

	public Map getMap() {
		return this.map;
	}

	public static List<GameObject> getGameObjectsList() {
		return gameObjectsBlueprintList;
	}

	public List<GameObject> getGameObjectsInstancesList() {
		return this.gameObjectsInstancesList;
	}

	public int getTotalTurns() {
		return this.totalTurns;
	}

	public List<CanMove> getMoveableGameObjectsList() {
		return this.moveableGameObjectsList;
	}

}
