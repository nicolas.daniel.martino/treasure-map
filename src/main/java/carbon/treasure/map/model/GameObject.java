package carbon.treasure.map.model;

import java.util.Comparator;

public abstract class GameObject {
	private String[] rawLine;
	private String name;
	private String description;
	private Position position;
	private boolean isObstacle;
	private boolean isReward;
	private String lineCharacter;
	private int reportOrder;

	protected Position createPositionFromRaw(String[] position) {
		return new Position(Integer.parseInt(position[1]), Integer.parseInt(position[2]));
	}

	public boolean addToTile(Tile tile) {
		return tile.getContent().add(this);
	}

	public boolean deleteFromTile(Tile tile) {
		return tile.getContent().remove(this);
	}

	public GameObject(String name, String description, boolean isObstacle, boolean isReward, String lineCharacter,
			int reportOrder) {
		this.name = name;
		this.description = description;
		this.isObstacle = isObstacle;
		this.isReward = isReward;
		this.lineCharacter = lineCharacter;
		this.reportOrder = reportOrder;
	}

	@Override
	public String toString() {
		return this.lineCharacter + " - " + this.position.getX() + " - " + this.position.getY();
	}

	public static Comparator<GameObject> orderForReport() {
		return Comparator.comparing(GameObject::getReportOrder);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isObstacle() {
		return this.isObstacle;
	}

	public void setObstacle(boolean isObstacle) {
		this.isObstacle = isObstacle;
	}

	public boolean isReward() {
		return this.isReward;
	}

	public void setReward(boolean isReward) {
		this.isReward = isReward;
	}

	public String getLineCharacter() {
		return this.lineCharacter;
	}

	public void setLineCharacter(String lineCharacter) {
		this.lineCharacter = lineCharacter;
	}

	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String[] getRawLine() {
		return this.rawLine;
	}

	public void setRawLine(String[] rawLine) {
		this.rawLine = rawLine;
	}

	public int getReportOrder() {
		return this.reportOrder;
	}

	public void setReportOrder(int reportOrder) {
		this.reportOrder = reportOrder;
	}

}
