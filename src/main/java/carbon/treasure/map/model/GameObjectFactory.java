package carbon.treasure.map.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import carbon.treasure.map.Cardinal;

public class GameObjectFactory {

	public List<GameObject> createFromRawLine(GameObject Blueprint, String[] cleanedSplittedLine) {
		if (Blueprint instanceof Mountain) {
			return Arrays.asList(this.parseMountain(cleanedSplittedLine));
		}
		if (Blueprint instanceof Treasure) {
			return this.parseTreasure(cleanedSplittedLine);
		}
		if (Blueprint instanceof Adventurer) {
			return Arrays.asList(this.parseAdventurer(cleanedSplittedLine));
		}
		return null;
	}

	private GameObject parseMountain(String[] splittedLine) {
		final Mountain mountain = new Mountain();
		mountain.setRawLine(splittedLine);

		// create position
		final Position goPosition = mountain.createPositionFromRaw(splittedLine);
		mountain.setPosition(goPosition);

		return mountain;
	}

	public List<GameObject> parseTreasure(String[] splittedLine) {
		final List<GameObject> treasureList = new ArrayList<>();

		// number of treasures is the last digit
		final int numberOfTreasures = Integer.parseInt(splittedLine[3]);
		for (int i = 0; i < numberOfTreasures; i++) {
			final Treasure treasure = new Treasure();
			// create position
			final Position goPosition = treasure.createPositionFromRaw(splittedLine);
			treasure.setPosition(goPosition);

			treasureList.add(treasure);
		}

		return treasureList;
	}

	public GameObject parseAdventurer(String[] splittedLine) {
		final Adventurer adventurer = new Adventurer();

		adventurer.setRawLine(splittedLine);
		adventurer.setFirstName(splittedLine[1]);
		adventurer.setOrientation(Cardinal.valueOf(splittedLine[4]));
		adventurer.setMovesList(Arrays.asList(splittedLine[5].split("")));
		// create position
		// position is not in the same spot for the adventurer
		final Position goPosition = adventurer
				.createPositionFromRaw(new String[] { null, splittedLine[2], splittedLine[3] });

		adventurer.setPosition(goPosition);

		// add to map
		return adventurer;
	}
}