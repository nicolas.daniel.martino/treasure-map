package carbon.treasure.map;

import carbon.treasure.map.view.Cli;

public class App {
	public static void main(String[] args) {
		System.out.println("Welcome to carbon it exercise, the treasure map.");
		final Cli gameIO = new Cli();
		// TODO deal with app args

		// parse map
		try {
			final Game game = gameIO.createGame(null);
			game.createFromFile();
			game.computeAllTurns();
			// create report
			final String report = game.generateGameReport();
			System.out.println("Game ended after: " + game.getTotalTurns() + " turns");
			System.out.println(report);
			gameIO.exportReport(report);
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("App shutdown gracefully");

	}

};