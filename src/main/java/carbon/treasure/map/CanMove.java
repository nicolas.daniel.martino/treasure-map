package carbon.treasure.map;

import carbon.treasure.map.model.Position;

public interface CanMove {
	public int getTotalTurns();

	public Position computeNextMove(int turn);

}
