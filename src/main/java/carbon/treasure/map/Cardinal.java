package carbon.treasure.map;

import carbon.treasure.map.model.Move;

public enum Cardinal {
	//@formatter:off
	N(new int[]{ 0, -1}),
	S(new int[]{ 0, 1}),
	E(new int[]{ 1, 0}),
	O(new int[]{ -1, 0});
	//@formatter:on
	private final int[] direction;

	Cardinal(int[] direction) {
		this.direction = direction;
	}

	public int[] getDirection() {
		return this.direction;
	}

	public static Cardinal computeNexOrientation(Cardinal orientation, Move move) {
		switch (orientation) {
		case N:
			switch (move) {
			case D:
				return Cardinal.E;
			case G:
				return Cardinal.O;
			default:
				return null;
			}
		case S:
			switch (move) {
			case D:
				return Cardinal.O;
			case G:
				return Cardinal.E;
			default:
				return null;
			}
		case E:
			switch (move) {
			case D:
				return Cardinal.S;
			case G:
				return Cardinal.N;
			default:
				return null;
			}
		case O:
			switch (move) {
			case D:
				return Cardinal.N;
			case G:
				return Cardinal.S;
			default:
				return null;
			}
		default:
			return null;
		}
	}

}
