package carbon.treasure.map.model;

/**
 * Inmovable gameObject that contains the parsing function to create the
 * mountain
 *
 * @author nicky
 *
 */
public class Mountain extends GameObject {

	public Mountain() {
		super("Mountain",
				"A large natural elevation of the earth's surface rising abruptly from the surrounding level; a large steep hill.",
				true, false, "M", 1);
		// TODO Auto-generated constructor stub
	}

}
