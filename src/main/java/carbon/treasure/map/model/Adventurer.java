package carbon.treasure.map.model;

import java.util.List;

import carbon.treasure.map.CanGetReward;
import carbon.treasure.map.CanMove;
import carbon.treasure.map.Cardinal;

/**
 * Inmovable gameObject that contains the parsing function to create the
 * mountain
 *
 * @author nicky
 *
 */
public class Adventurer extends GameObject implements CanMove, CanGetReward {

	private int rewards;
	private String firstName;
	private Cardinal orientation;
	private List<String> movesList;

	@Override
	public String toString() {
		return this.getLineCharacter() + " - " + this.getPosition().getX() + " - " + this.getPosition().getY() + " - "
				+ this.orientation.name() + " - " + this.rewards;
	}

	@Override
	public int getTotalTurns() {
		return this.movesList.size();
	}

	@Override
	public Position computeNextMove(int turn) {
		final Move move = Move.valueOf(this.movesList.get(turn));
		if (move.equals(Move.A)) {
			return this.getPosition().computeNextPosition(this.getOrientation());
		}
		this.setOrientation(Cardinal.computeNexOrientation(this.getOrientation(), move));
		return null;
	}

	public Adventurer() {
		super("Adventurer", "A person who enjoys or seeks adventure", false, true, "A", 3);
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Cardinal getOrientation() {
		return this.orientation;
	}

	public void setOrientation(Cardinal orientation) {
		this.orientation = orientation;
	}

	public List<String> getMovesList() {
		return this.movesList;
	}

	public void setMovesList(List<String> movesList) {
		this.movesList = movesList;
	}

	@Override
	public int getCurrentRewards() {
		return this.rewards;
	}

	@Override
	public void addOneReward() {
		this.rewards++;
	}

}
