package carbon.treasure.map.model;

public enum Move {

	A, // avance tout droit
	G, // gauche
	D // droite
	;

}
