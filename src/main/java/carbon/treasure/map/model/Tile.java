package carbon.treasure.map.model;

import java.util.ArrayList;
import java.util.List;

public class Tile {
	List<GameObject> content = new ArrayList<>();

	public List<GameObject> getContent() {
		return this.content;
	}

	public void addContent(GameObject content) {
		this.content.add(content);
	}

}
